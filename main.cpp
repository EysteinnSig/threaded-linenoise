/*
 * main.cpp
 *
 *  Created on: Jan 13, 2020
 *      Author: eysteinn
 */
#define _XOPEN_SOURCE 700

#include <iostream>
#include <string>
#include <thread>
#include <atomic>
#include <regex>

//#include "utf8proc.h"
#include "threaded-linenoise.hpp"



static std::atomic<bool> threadexit;

void worker()
{
	int tick =0;
	while (true)
	{
		int max_columns = linenoise::getColumns();
		int cursor_columns = linenoise::ColLength(linenoise::globalState.text, linenoise::globalState.current_pos)+linenoise::ColLength(linenoise::ReplaceAnsiSeq(linenoise::globalState.prompt)); //linenoise::getCursorColumn();
		int total_length = linenoise::ColLength(linenoise::ReplaceAnsiSeq(linenoise::globalState.prompt))+
				linenoise::ColLength(linenoise::globalState.text);
		linenoise::Print("Tick: " +std::to_string(tick++)+
				"   CursorCol: "+std::to_string(cursor_columns)+
				"    MaxCol: "+std::to_string(max_columns)+
				"\tCharCnt: "+std::to_string(linenoise::globalState.text.length())+
				"\tTextLen: "+std::to_string(linenoise::ColLength(linenoise::globalState.text))+
				"\tTotalLen: "+std::to_string(total_length)
		);
		//printf("Tick: %i\n", tick++);
		std::this_thread::sleep_for(std::chrono::duration<double>(1.0/10.0));
		if (threadexit)
			break;
	}
}

int count(std::string str)
{
	char *s=&str[0];
	int len = 0;
	while (*s) len += (*s++ & 0xc0) != 0x80;
	return len;
}

void testUnicode()
{
	//std::u32string str = (char32_t*)"\xA0"; //\xE2\x98\xA0";
	std::u32string str32 = U"\xE2\x98\xA0";
	//std::u32string str = (char32_t*)"☠"; //U"\xA0\x98\xE2";
	//std::u32string str32 = U"AB";
	printf("%i\n", str32.length());
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> convert;

	std::string str8 = "\xE2\x98\xA0"; //convert.to_bytes(str32);
	//str.append((char32_t*)"\x00\xE2\x98\xA0");
	printf("length: %i\t%i\n", str8.length(), str32.length());
	printf("\xE2\x98\xA0");
	printf("%s\n", str32.c_str());
	printf("%s\n", str8.c_str());
	exit(0);

}


void test1()
{
	/*std::string tmp1 = "linenoise>";
	std::string tmp2 = "\033[31;1;4mHello\033[0m";//"\x1b[1;32mlinenoise\x1b[0m>";
	printf("%s\n", tmp2.c_str());
	std::u32string tmp3; // = (char32_t*)"\x1b[1;32mlinenoise\x1b[0m>";
	tmp3.append(tmp2.begin(), tmp2.end());
	std::wstring tmp4 = (wchar_t*)"\x1b[1;32mlinenoise\x1b[0m>";
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
	printf("Char Length: %i\n", conv.from_bytes(tmp2).size());
	printf("wchar: %i\n", tmp4.length());
	printf("count: %i\n", count(tmp2));
	printf("tmp3.length() = %i\n", tmp3.length());
	int i = std::wstring_convert< std::codecvt_utf8<char32_t>, char32_t >().from_bytes(tmp2).size();
	printf("%i\n", i);
	printf("%i\t%i\t%i\n", tmp1.length(), tmp2.length(), tmp2.size());
	assert(tmp1.length() == 10);
	assert(tmp2.length() == 10);*/
}
void testUnicode2()
{

	std::u32string str32 = U"þæað☠";
	str32.insert(2, U"☠");

	str32.insert(0, (char32_t*)U"æ");
	std::string str8;

	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
	char c4[4]={(char)255, (char)255, (char)255, (char)255};
	std::u32string s = conv.from_bytes(c4);
	printf("s.length: %i\n", s.length());
	printf("32string len: %i \nstring len: %i\n", str32.length(), conv.to_bytes(str32).length());
	printf("32string: %s\nstring: %s\n", str32.c_str(), conv.to_bytes(str32).c_str());
	str8 = "þ";
	printf("%i\n", str8.length());
	exit(0);
}

void testUnicode3()
{
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
	std::string s8 = "Hallカo☠heimur";

	std::u32string s32;
	s32 = conv.from_bytes(s8);
	printf("%s\n", conv.to_bytes(s32).c_str());
	exit(0);
}

void testUnicode4()
{
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;

	std::u32string str32 = U"カ"; //U"⭕☢";

	int l = linenoise::mk_wcwidth(str32[0]);
	//int l = utf8proc_charwidth((int)str32[0]);
	printf("string: >%s<\n", conv.to_bytes(str32).c_str());
	printf("length: %i\n", l);

	exit(0);
}

std::string ReplaceAnsi(const std::string &str, const std::string &newstr = "")
{
	std::regex e("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]");
	std::string tmp = std::regex_replace(str, e, newstr);
	return tmp;
}

std::u32string ReplaceAnsi(const std::u32string &str, const std::u32string &newstr = U"")
{
	// Ugly hack since std::basic_regex<char32> does not work
	return linenoise::utf_converter.from_bytes(ReplaceAnsi(linenoise::utf_converter.to_bytes(str)));
}

std::u32string remAnsi(const std::u32string &str32)
{
	std::string str8 = linenoise::utf_converter.to_bytes(str32);
	std::regex e("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]");
	return linenoise::utf_converter.from_bytes(std::regex_replace(str8, e, ""));
}
void testRegex()
{
	/*std::basic_regex<char32_t> br; //U"(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]");
	br = U"([@-~])";
	std::u32string str32 = U"\x1b[1;32mlinenoise\x1b[0m> ";
	printf("Regex finished\n");
	std::u32string out32 = std::regex_replace(str32, br, U"!");
	printf("%s\n", linenoise::utf_converter.to_bytes(out32).c_str());*/
	//printf("%s\n", linenoise::utf_converter.to_bytes(remAnsi(U"\x1b[1;32mlinenoise\x1b[0m> ")).c_str());
	std::u32string str32 = U"\x1b[1;32mlineカo☠noise\x1b[0m> ";
	std::u32string after32 = ReplaceAnsi(str32);
	printf("%s\n", linenoise::utf_converter.to_bytes(str32).c_str());
	printf("%s\n", linenoise::utf_converter.to_bytes(after32).c_str());

	std::string str8 = "\x1b[1;32mlinenoise\x1b[0m> ";
	std::regex e("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]");
	std::string tmp = std::regex_replace(str8, e, "!");
	printf("%s\n", tmp.c_str());

	printf(ReplaceAnsi(str8).c_str());
	exit(0);

}

int main(int argc, char *argv[]) {
	//test1();
	//testUnicode();
	//testUnicode2();
	//testUnicode3();
	//testUnicode4();
	//testRegex();

	std::cout << "This is test for linenoise" << std::endl;
	//linenoise::SetMultiLine(false);

	threadexit = false;
	std::thread t1(worker);
	bool quit;
	std::string cmd;

	linenoise::LoadHistory("history.txt");

	while (quit==false) {

		quit = linenoise::Readline("\x1b[1;32mlinenoise\x1b[0m> ", cmd);
		if (!cmd.empty())
			linenoise::AddHistory(cmd);
		//quit = linenoise::Readline("linenoise> ", cmd);
		//quit = linenoise::Readline("", cmd);
		printf("Command: >%s<\n", cmd.c_str());
		//printf("%s\n", quit?"Quitting":"Not quitting");

	}
	linenoise::SaveHistory("history.txt");
	printf("Done\n");
	threadexit = true;
	t1.join();
	return 0;
}
