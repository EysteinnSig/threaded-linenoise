/*
 *  linenoise.hpp -- Multi-platfrom C++ header-only linenoise library.
 *
 *  All credits and commendations have to go to the authors of the
 *  following excellent libraries.
 *
 *  - linenoise.h and linenose.c (https://github.com/antirez/linenoise)
 *  - ANSI.c (https://github.com/adoxa/ansicon)
 *  - Win32_ANSI.h and Win32_ANSI.c (https://github.com/MSOpenTech/redis)
 *
 * ------------------------------------------------------------------------
 *
 *  Copyright (c) 2015 yhirose
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* linenoise.h -- guerrilla line editing library against the idea that a
 * line editing lib needs to be 20,000 lines of C code.
 *
 * See linenoise.c for more information.
 *
 * ------------------------------------------------------------------------
 *
 * Copyright (c) 2010, Salvatore Sanfilippo <antirez at gmail dot com>
 * Copyright (c) 2010, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ANSI.c - ANSI escape sequence console driver.
 *
 * Copyright (C) 2005-2014 Jason Hood
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the author be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Jason Hood
 * jadoxa@yahoo.com.au
 */

/*
 * Win32_ANSI.h and Win32_ANSI.c
 *
 * Derived from ANSI.c by Jason Hood, from his ansicon project (https://github.com/adoxa/ansicon), with modifications.
 *
 * Copyright (c), Microsoft Open Technologies, Inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LINENOISE_HPP
#define LINENOISE_HPP
#include <regex>


#ifndef _WIN32
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#else
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#include <io.h>
#ifndef STDIN_FILENO
#define STDIN_FILENO (_fileno(stdin))
#endif
#ifndef STDOUT_FILENO
#define STDOUT_FILENO 1
#endif
#define isatty _isatty
#define write win32_write
#define read _read
#pragma warning(push)
#pragma warning(disable : 4996)
#endif
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <string>
#include <fstream>
#include <functional>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <locale>
#include <codecvt>
#include <cassert>
#include <csignal>
#include <cstring>
#include <list>
#include <regex>
#include "wcwidth.cpp"

namespace linenoise {

enum KEY_ACTION {
	KEY_NULL = 0,       /* NULL */
	CTRL_A = 1,         /* Ctrl+a */
	CTRL_B = 2,         /* Ctrl-b */
	CTRL_C = 3,         /* Ctrl-c */
	CTRL_D = 4,         /* Ctrl-d */
	CTRL_E = 5,         /* Ctrl-e */
	CTRL_F = 6,         /* Ctrl-f */
	CTRL_H = 8,         /* Ctrl-h */
	TAB = 9,            /* Tab */
	CTRL_K = 11,        /* Ctrl+k */
	CTRL_L = 12,        /* Ctrl+l */
	ENTER = 13,         /* Enter */
	CTRL_N = 14,        /* Ctrl-n */
	CTRL_P = 16,        /* Ctrl-p */
	CTRL_T = 20,        /* Ctrl-t */
	CTRL_U = 21,        /* Ctrl+u */
	CTRL_W = 23,        /* Ctrl+w */
	ESC = 27,           /* Escape */
	BACKSPACE =  127    /* Backspace */
};
static const char *unsupported_term[] = {"dumb","cons25","emacs",NULL};
static bool atexit_registered = false; /* Register atexit just 1 time. */

#ifndef _WIN32
static struct termios orig_termios; /* In order to restore at exit.*/
#endif
static bool rawmode = false; /* For atexit() function to check if restore is needed*/
std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> utf_converter;


static std::list<std::u32string> history; // = {U"test1", U"test2", U"test3"};
static int history_length = 30;

struct linenoiseState {
	int stdin_fd;
	int stdout_fd;
	std::u32string prompt;
	std::u32string text;
	std::mutex mutex;
	int current_pos = 0;
	bool multiline = true;
	int current_history2;
	std::list<std::u32string>::iterator current_history = history.end();
} globalState;



//// History

static std::string AggregateHistory()
{
	std::string txt = "";
	for (auto &line : history)
	{
		txt = utf_converter.to_bytes(line) + "\n";
	}
	return txt;
}


static void AddHistory(const std::u32string &text)
{
	//history.push_front(text);
	history.push_back(text);
	while (history_length >= 0 && history_length < history.size())
	{
		history.pop_front();
	}
	globalState.current_history = history.end();
}
static void AddHistory(const std::string &text)
{
	AddHistory(linenoise::utf_converter.from_bytes(text));
}
static std::u32string NextHistory()
{
	if (history.empty()) return U"";

	auto &current = globalState.current_history;

	if (current != history.begin())
	{
		//current = history.end();
		current--;
	}
	return *current;

	/*if (current != history.end())
		current++;

	if (current == history.end())
		current = history.begin();*/

	//return *current;
}
static std::u32string PrevHistory()
{
	if (history.empty()) return U"";

	auto &current = globalState.current_history;
	if (current!=history.end())
		current++;
	if (current==history.end())
		return U"";
	return *current;
	/*auto &current = globalState.current_history;
	if (current == history.begin())
		current = history.end();

	current--;

	return *current;*/
}

static void SaveHistory(const std::string &filename)
{
	std::ofstream file(filename, std::ofstream::trunc);
	if (file.is_open())
	{
		for (auto line : history)
		{
			file << utf_converter.to_bytes(line) << '\n';
		}
		file.close();
	}
}

static void LoadHistory(const std::string &filename)
{
	std::ifstream file(filename);
	if (file.is_open()) {
	    std::string line;
	    while (getline(file, line)) {
	        // using printf() in all tests for consistency
	        //printf("%s", line.c_str());
	    	AddHistory(line);
	    }
	    file.close();
	}
}

////

/* Convert UTF8 to Unicode code point
 */
inline int unicodeUTF8CharToCodePoint(
   const char* buf,
   int         len,
   int*        cp)
{
    if (len) {
        unsigned char byte = buf[0];
        if ((byte & 0x80) == 0) {
            *cp = byte;
            return 1;
        } else if ((byte & 0xE0) == 0xC0) {
            if (len >= 2) {
                *cp = (((unsigned long)(buf[0] & 0x1F)) << 6) |
                       ((unsigned long)(buf[1] & 0x3F));
                return 2;
            }
        } else if ((byte & 0xF0) == 0xE0) {
            if (len >= 3) {
                *cp = (((unsigned long)(buf[0] & 0x0F)) << 12) |
                      (((unsigned long)(buf[1] & 0x3F)) << 6) |
                       ((unsigned long)(buf[2] & 0x3F));
                return 3;
            }
        } else if ((byte & 0xF8) == 0xF0) {
            if (len >= 4) {
                *cp = (((unsigned long)(buf[0] & 0x07)) << 18) |
                      (((unsigned long)(buf[1] & 0x3F)) << 12) |
                      (((unsigned long)(buf[2] & 0x3F)) << 6) |
                       ((unsigned long)(buf[3] & 0x3F));
                return 4;
            }
        }
    }
    return 0;
}

/* Read UTF8 character from file.
 */
inline int unicodeReadUTF8Char(int fd, char* buf, int* cp)
{
    int nread = read(fd,&buf[0],1);

    if (nread <= 0) { return nread; }

    unsigned char byte = buf[0];

    if ((byte & 0x80) == 0) {
        ;
    } else if ((byte & 0xE0) == 0xC0) {
        nread = read(fd,&buf[1],1);
        if (nread <= 0) { return nread; }
    } else if ((byte & 0xF0) == 0xE0) {
        nread = read(fd,&buf[1],2);
        if (nread <= 0) { return nread; }
    } else if ((byte & 0xF8) == 0xF0) {
        nread = read(fd,&buf[1],3);
        if (nread <= 0) { return nread; }
    } else {
        return -1;
    }

    return unicodeUTF8CharToCodePoint(buf, 4, cp);
}


inline size_t getTotalLength()
{
	return globalState.prompt.length() + globalState.text.length();
}

/* Use the ESC [6n escape sequence to query the horizontal cursor position
 * and return it. On error -1 is returned, on success the position of the
 * cursor. */
/*inline int getCursorColumn() {
	//return globalState.currentCol;
	return globalState.current_pos
}*/


/* Try to get the number of columns in the current terminal, or assume 80
 * if it fails. */
inline int getColumns(/*int ifd, int ofd*/) {
	int ifd = globalState.stdin_fd;
	int ofd = globalState.stdout_fd;
#ifdef _WIN32
    CONSOLE_SCREEN_BUFFER_INFO b;

    if (!GetConsoleScreenBufferInfo(hOut, &b)) return 80;
    return b.srWindow.Right - b.srWindow.Left;
#else
    struct winsize ws;

    int ret = ioctl(1, TIOCGWINSZ, &ws);
    assert(ret != -1);
    return ws.ws_col;
    assert(ioctl(1, TIOCGWINSZ, &ws) != -1 && ws.ws_col != 0);
    return ws.ws_col;


failed:
    return 80;
#endif
}
inline int getRequiredRows()
{
	size_t length = globalState.prompt.length() + globalState.text.length();
	return length/getColumns()+1;
}


std::string ReplaceAnsiSeq(const std::string &str, const std::string &newstr = "")
{
	std::regex e("(\\x9B|\\x1B\\[)[0-?]*[ -\\/]*[@-~]", std::regex_constants::basic);
	std::string tmp = std::regex_replace(str, e, newstr);
	return tmp;
}

std::u32string ReplaceAnsiSeq(const std::u32string &str, const std::u32string &newstr = U"")
{
	// Ugly hack since std::basic_regex<char32> does not work
	return linenoise::utf_converter.from_bytes(ReplaceAnsiSeq(linenoise::utf_converter.to_bytes(str)));
}

inline int ColLength(const std::u32string& str, int endpos = -1)
{
	//assert(endpos < str.length());
	endpos = std::min(endpos, (int)str.length());
	if (endpos < 0) endpos = str.length();
	int len=0;
	for (int k = 0; k< endpos; k++)
	{
		char32_t c = str[k];
		int charlen = linenoise::mk_wcwidth(c);
		if (charlen >0)
			len += charlen;
	}
	/*int l = linenoise::mk_wcswidth(str.data(), endpos);
	l=(l<0? 0: l);
	printf(">%i\n", endpos);*/
	return len;
}

inline void indent2RowCol(int indent, int &row, int &col)
{
	int max_columns = getColumns();
	row = indent / max_columns;
	col = indent % max_columns;
}
inline void moveCursor(int drow, int dcol)
{
	/*int drow = new_row-old_row;
	int dcol = new_col-old_col;*/
	std::string cmd="";
	if (dcol < 0)
		cmd.append("\x1b["+std::to_string(-dcol)+"D");  // Move cursor left
	if (dcol > 0)
		cmd.append("\x1b["+std::to_string(dcol)+"C");	// Move cursor right
	if (drow < 0)
		cmd.append("\x1b["+std::to_string(-drow)+"A");	// Move cursor up
	if (drow > 0)
		cmd.append("\x1b["+std::to_string(drow)+"B");	// Move cursor down
	write(globalState.stdout_fd, cmd.c_str(), cmd.length());
}
inline void moveCursor(int new_row, int new_col, int old_row, int old_col)
{
	moveCursor(new_row-old_row, new_col-old_col);
}
inline void moveCursorIndent(int new_indent, int old_indent)
{
	int new_row, new_col, old_row, old_col;
	indent2RowCol(new_indent, new_row, new_col);
	indent2RowCol(old_indent, old_row, old_col);
	moveCursor(new_row, new_col, old_row, old_col);
}

inline int GetCursorIndent()
{
	int prompt_len = ColLength(ReplaceAnsiSeq(globalState.prompt));
	int text_len = ColLength(globalState.text, globalState.current_pos);
	return prompt_len+text_len;
}
inline void eraseLine()
{

	if (globalState.multiline) {
		std::string debug;
		int maxColumns = getColumns();
		/*int totalLength = globalState.text.length()+globalState.prompt.length();
		int lineCount = totalLength / maxColumns + 1;
		int lineIndex = (globalState.text.length() + globalState.current_pos) / maxColumns;*/

		int totalLength = ColLength(globalState.text)+ColLength(ReplaceAnsiSeq(globalState.prompt));
		int lineCount = totalLength / maxColumns + 1;
		int lineIndex = (ColLength(ReplaceAnsiSeq(globalState.prompt)) + ColLength(globalState.text, globalState.current_pos)) / maxColumns;
		//int lineIndex = (ColLength(globalState.text) + globalState.current_pos) / maxColumns;
		write(globalState.stdout_fd, "\x1b[1000D",7);		// Move to start of line
		for (int k=lineIndex; k<lineCount-1; k++)
		{
			debug += "Down\n";
			write(globalState.stdout_fd, "\x1b[1B", 4);		// Move down, one line at a time
		}
		for (int k=0; k<lineCount-1; k++)
		{
			debug += "Clear\n";
			write(globalState.stdout_fd, "\x1b[2K",4);		// Erase whole line
			debug += "Up\n";
			write(globalState.stdout_fd, "\x1b[1A", 4);		// Move one line up
		}
		write(globalState.stdout_fd, "\x1b[2K",4);		// Erase whole line
		debug += "Clear\n";
		debug += "TotalLength: " + std::to_string(totalLength)+ "    LineCount: " + std::to_string(lineCount) + "    lineIndex: "+std::to_string(lineIndex)+"\n";
		//write(globalState.stdout_fd, "\x1b[2K",4);		// Move to start of line
		debug += std::to_string(totalLength)+"\n";
		//printf("\n\n%s\n\n", debug.c_str());
	}
	else {
		assert(false);
		write(STDOUT_FILENO,"\x1b[2K",4);		// Erase whole line
		write(STDOUT_FILENO,"\x1b[1000D", 7);	// Move to start of line
	}
}

inline void writeLine()
{
	std::u32string txt = globalState.prompt + globalState.text;
	int columns = getColumns();

	if (globalState.multiline)
	{
		std::string tmp = utf_converter.to_bytes(txt);
		//tmp += " ";

		//write(globalState.stdin_fd, txt.c_str(), txt.length());
		write(globalState.stdin_fd, tmp.c_str(), tmp.length());

		//printf("%s", tmp.c_str());
		//printf("Printed %i\n", ColLength(linenoise::ReplaceAnsiSeq(txt)));
		//moveCursorIndent(globalState.prompt.length()+globalState.current_pos, txt.length());

		// After writing, cursor is at the end of the line.  Need to move the old cursor pos.
		int new_indent = ColLength(ReplaceAnsiSeq(globalState.prompt))+ColLength(globalState.text, globalState.current_pos);
		int old_indent = ColLength(ReplaceAnsiSeq(txt));
		char c = '\n';
		if (old_indent % columns == 0)
			write(globalState.stdin_fd, &c, 1);
		//printf("%i <- %i\n", new_indent, old_indent);
		/*moveCursorIndent(ColLength(globalState.prompt)+ColLength(globalState.text, globalState.current_pos),
				ColLength(txt));*/
		moveCursorIndent(new_indent, old_indent);
		//printf("%i\t%i\t%i\n", ColLength(globalState.prompt)+ColLength(globalState.text, globalState.current_pos), ColLength(txt), txt.length());
	}
	else
	{
		assert(false);
	}
}
inline void Print(const std::string &msg)
{
	std::lock_guard<std::mutex> lock(globalState.mutex);
	eraseLine();
	printf("%s\n", msg.c_str());
	writeLine();
}

inline void SetPrompt(const std::u32string &prompt)
{
	std::lock_guard<std::mutex> lock(globalState.mutex);
	globalState.prompt = prompt;
}

inline void linenoiseEdit(int stdin_fd, int stdout_fd, bool &quit, std::u32string &txt)
{
	//txt.clear();

	{
		std::lock_guard<std::mutex> lock(globalState.mutex);
		globalState.text.clear();
		writeLine();
	}

	while (true)
	{
		int c;
		char cbuf[4];
		int nread;
		char seq[3];
		cbuf[0] = cbuf[1] = cbuf[2] = cbuf[3] = 0;

#ifdef _WIN32
        nread = win32read(&c);
        if (nread == 1) {
            cbuf[0] = c;
        }
#else
        nread = unicodeReadUTF8Char(stdin_fd,cbuf,&c);
#endif
        if (nread <= 0) return;
        {
        std::lock_guard<std::mutex> lock(globalState.mutex);
        switch(c) {
			case ENTER:    /* enter */
				//if (!history.empty()) history.pop_back();
				txt = globalState.text;
				globalState.text.clear();// = U"";
				globalState.current_pos = 0;
				globalState.current_history = history.end();
				//printf("\n");
				return;
			case CTRL_C:     /* ctrl-c */
				txt = U"";
				quit = true;
				return;
			case BACKSPACE:   /* backspace */
			case 8:     /* ctrl-h */
				if (globalState.current_pos<=0)
					break;
				eraseLine();
				globalState.text.erase(globalState.current_pos-1, 1);
				moveCursorIndent(ColLength(globalState.text, globalState.current_pos-1), ColLength(globalState.text, globalState.current_pos));
				globalState.current_pos--;
				writeLine();
				break;
			case ESC:    /* escape sequence */
				/* Read the next two bytes representing the escape sequence.
				 * Use two calls to handle slow terminals returning the two
				 * chars at different times. */
				if (read(globalState.stdin_fd,seq,1) == -1) break;
				if (read(globalState.stdin_fd,seq+1,1) == -1) break;

				/* ESC [ sequences. */
				if (seq[0] == '[') {
					if (seq[1] >= '0' && seq[1] <= '9') {
						/* Extended escape, read additional byte. */
						if (read(globalState.stdin_fd,seq+2,1) == -1) break;
						if (seq[2] == '~') {
							switch(seq[1]) {
							case '3': /* Delete key. */
								eraseLine();
								globalState.text.erase(globalState.current_pos, 1);
								/*moveCursorIndent(ColLength(globalState.text, globalState.current_pos-1),
										ColLength(globalState.text, globalState.current_pos));
								globalState.current_pos--;*/
								writeLine();
								break;
							}
						}
					} else {
						switch(seq[1]) {
						case 'A': /* Up */
						{
							eraseLine();
							globalState.text = NextHistory();
							txt = globalState.text;
							globalState.current_pos = std::min((int)globalState.text.length(), globalState.current_pos);;
							writeLine();
							//linenoiseEditHistoryNext(&l, LINENOISE_HISTORY_PREV);
							break;
						}
						case 'B': /* Down */
							eraseLine();
							globalState.text = PrevHistory();
							txt = globalState.text;
							globalState.current_pos = std::min((int)globalState.text.length(), globalState.current_pos);
							writeLine();
							//linenoiseEditHistoryNext(&l, LINENOISE_HISTORY_NEXT);
							break;
						case 'C': /* Right */
						{
							if ((int)globalState.text.length() < globalState.current_pos+1)
								break;
							//linenoiseEditMoveRight(&l);
							int old_cursor_indent; // = globalState.prompt.length()+globalState.current_pos;
							int new_cursor_indent; // = old_cursor_indent+1;

							/*old_cursor_indent = ColLength(globalState.prompt) +
									ColLength(globalState.text, globalState.current_pos);
							new_cursor_indent = ColLength(globalState.prompt) +
									ColLength(globalState.text, globalState.current_pos+1);*/
							old_cursor_indent = linenoise::GetCursorIndent();
							globalState.current_pos++;
							new_cursor_indent = GetCursorIndent();

							moveCursorIndent(new_cursor_indent, old_cursor_indent);
							//globalState.current_pos++;
							break;
						}
						case 'D': /* Left */
						{
							if (globalState.current_pos<=0)
								break;

							int old_cursor_indent; // = globalState.prompt.length()+globalState.current_pos;
							int new_cursor_indent; // = old_cursor_indent-1;

							/*old_cursor_indent = ColLength(ReplaceAnsiSeq(globalState.prompt)) + ColLength(globalState.text, globalState.current_pos);
							new_cursor_indent = ColLength(ReplaceAnsiSeq(globalState.prompt)) + ColLength(globalState.text, globalState.current_pos-1);*/
							old_cursor_indent = GetCursorIndent();
							globalState.current_pos--;
							new_cursor_indent = GetCursorIndent();

							moveCursorIndent(new_cursor_indent, old_cursor_indent);
							//globalState.current_pos--;
							/*int newindent = globalState.prompt.length()+globalState.current_pos-1;
							int oldindent = globalState.prompt.length()+globalState.current_pos;
							int old_row, old_col, new_row, new_col;
							indent2RowCol(oldindent, old_row, old_col);
							indent2RowCol(newindent, new_row, new_col);

							moveCursor(new_row-old_row, new_col-old_col);
							globalState.current_pos--;*/
							//linenoiseEditMoveLeft(&l);
							break;
						}
						case 'H': /* Home */
						{
							int old_cursor_indent = GetCursorIndent();
							globalState.current_pos = 0;
							int new_cursor_indent = GetCursorIndent();
							moveCursorIndent(new_cursor_indent, old_cursor_indent);
							//linenoiseEditMoveHome(&l);
							break;
						}
						case 'F': /* End*/
						{
							int old_cursor_indent = GetCursorIndent();
							globalState.current_pos = globalState.text.length();
							int new_cursor_indent = GetCursorIndent();
							moveCursorIndent(new_cursor_indent, old_cursor_indent);

							break;
						}
						}
					}
				}

				/* ESC O sequences. */
				else if (seq[0] == 'O') {
					switch(seq[1]) {
					case 'H': /* Home */
						//linenoiseEditMoveHome(&l);
						break;
					case 'F': /* End*/
						//linenoiseEditMoveEnd(&l);
						break;
					}
				}
				break;
			default:

				std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;

				std::u32string s = conv.from_bytes(cbuf, cbuf+nread);
				//printf("Hallo: %i\n", s.length());
				//printf("Codepoint: %X\n", (wchar_t)c);
				//unicodeUTF8CharToCodePoint(buf, nread, )
				//unicodeUTF8CharToCodePoint(buf, 4, &cp);
				//printf("width: %i\n", mk_wcswidth(s.data(), s.length()));

				//printf("width: %i\n", mk_wcwidth((wchar_t)c));
				/*std::u32string tmp;
				char32_t c32;
				std::memcpy(&c32, cbuf, 4);
				tmp.append(&c32);*/
				/*printf("\n\nlen: %i, nread: %i\n\n", s.size(), nread);
				printf("%X %X %X %X\n\n", (uint8_t)cbuf[0], (uint8_t)cbuf[1], (uint8_t)cbuf[2], (uint8_t)cbuf[3]);*/
				/*std::string tmp(cbuf, nread);
				printf("\n\n>>%s<<\t%i\n\n", tmp.c_str(), nread);*/ //☠
				//globalState.text.insert(globalState.current_pos, cbuf, nread);
				globalState.text.insert(globalState.current_pos, s);
				//txt.append(cbuf, nread);

				//write(stdin_fd, cbuf, nread);
				//globalState.text = txt;
				//globalState.current_pos += mk_wcwidth((char32_t)c);
				globalState.current_pos++;
				eraseLine();
				writeLine();
				//printf("%c", c);
        }
        }
	}
	printf("Leaving linenoiseEdit\n");
	return;
}

/* Return true if the terminal name is in the list of terminals we know are
* not able to understand basic escape sequences. */
inline bool isUnsupportedTerm(void) {
#ifndef _WIN32
	char *term = getenv("TERM");
	int j;

	if (term == NULL) return false;
	for (j = 0; unsupported_term[j]; j++)
		if (!strcasecmp(term,unsupported_term[j])) return true;
#endif
	return false;
}


inline void disableRawMode(int fd) {
#ifdef _WIN32
    if (consolemodeIn) {
      SetConsoleMode(hIn, consolemodeIn);
      consolemodeIn = 0;
    }
    rawmode = false;
#else
    /* Don't even check the return value as it's too late. */
    if (rawmode && tcsetattr(fd,TCSAFLUSH,&orig_termios) != -1)
        rawmode = false;
#endif
}

/* At exit we'll try to fix the terminal to the initial conditions. */
inline void linenoiseAtExit(void) {
    disableRawMode(STDIN_FILENO);
}

/* Raw mode: 1960 magic shit. */
inline bool enableRawMode(int fd) {
#ifndef _WIN32
    struct termios raw;

    if (!isatty(STDIN_FILENO)) goto fatal;
    if (!atexit_registered) {
        atexit(linenoiseAtExit);
        atexit_registered = true;
    }
    if (tcgetattr(fd,&orig_termios) == -1) goto fatal;

    raw = orig_termios;  /* modify the original mode */
    /* input modes: no break, no CR to NL, no parity check, no strip char,
     * no start/stop output control. */
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    /* output modes - disable post processing */
    // NOTE: Multithreaded issue #20 (https://github.com/yhirose/cpp-linenoise/issues/20)
    // raw.c_oflag &= ~(OPOST);
    /* control modes - set 8 bit chars */
    raw.c_cflag |= (CS8);
    /* local modes - echoing off, canonical off, no extended functions,
     * no signal chars (^Z,^C) */
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    /* control chars - set return condition: min number of bytes and timer.
     * We want read to return every single byte, without timeout. */
    raw.c_cc[VMIN] = 1; raw.c_cc[VTIME] = 0; /* 1 byte, no timer */

    /* put terminal in raw mode after flushing */
    if (tcsetattr(fd,TCSAFLUSH,&raw) < 0) goto fatal;
    rawmode = true;
#else
    if (!atexit_registered) {
        /* Cleanup them at exit */
        atexit(linenoiseAtExit);
        atexit_registered = true;

        /* Init windows console handles only once */
        hOut = GetStdHandle(STD_OUTPUT_HANDLE);
        if (hOut==INVALID_HANDLE_VALUE) goto fatal;
    }

    DWORD consolemodeOut;
    if (!GetConsoleMode(hOut, &consolemodeOut)) {
        CloseHandle(hOut);
        errno = ENOTTY;
        return false;
    };

    hIn = GetStdHandle(STD_INPUT_HANDLE);
    if (hIn == INVALID_HANDLE_VALUE) {
        CloseHandle(hOut);
        errno = ENOTTY;
        return false;
    }

    GetConsoleMode(hIn, &consolemodeIn);
    DWORD consolemodeInWithRaw = consolemodeIn & ~ENABLE_PROCESSED_INPUT;
    SetConsoleMode(hIn, consolemodeInWithRaw);

    rawmode = true;
#endif
    return true;

fatal:
    errno = ENOTTY;
    return false;
}


/* This function calls the line editing function linenoiseEdit() using
 * the STDIN file descriptor set in raw mode. */
inline bool linenoiseRaw(const std::string &prompt, std::string& line) {
    bool quit = false;

    if (!isatty(STDIN_FILENO)) {
        /* Not a tty: read from file / pipe. */
        std::getline(std::cin, line);
    } else {
        /* Interactive editing. */
        if (enableRawMode(STDIN_FILENO) == false) {
            return quit;
        }
        std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> convertor;

        SetPrompt(convertor.from_bytes(prompt));
        std::u32string tmp32;
        linenoiseEdit(STDIN_FILENO, STDOUT_FILENO, quit, tmp32);
        line = convertor.to_bytes(tmp32);
        //char buf[LINENOISE_MAX_LINE];

        /*auto count = linenoiseEdit(STDIN_FILENO, STDOUT_FILENO, buf, LINENOISE_MAX_LINE, prompt);
        if (count == -1) {
            quit = true;
        } else {
            line.assign(buf, count);
        }*/

        disableRawMode(STDIN_FILENO);
        printf("\n");
    }
    return quit;
}


 /* The high level function that is the main API of the linenoise library.
  * This function checks if the terminal has basic capabilities, just checking
  * for a blacklist of stupid terminals, and later either calls the line
  * editing function or uses dummy fgets() so that you will be able to type
  * something even in the most desperate of the conditions. */
inline bool Readline(const std::string &prompt, std::string& line) {
	if (isUnsupportedTerm()) {
		fflush(stdout);
		std::getline(std::cin, line);
		return false;
	} else {
		/*std::u32string tmp;
		auto quit = linenoiseRaw(prompt, tmp);
		std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
		line = conv.to_bytes(tmp);*/

		//return quit;
		return linenoiseRaw(prompt, line);
	}
}

inline std::string Readline(const std::string &prompt, bool& quit) {
	std::string line;
	quit = Readline(prompt, line);
	return line;
}

inline std::string Readline(const std::string &prompt) {
	bool quit; // dummy
	return Readline(prompt, quit);
}





} // namespace linenoise

#ifdef _WIN32
#undef isatty
#undef write
#undef read
#pragma warning(pop)
#endif

#endif /* __LINENOISE_HPP */
